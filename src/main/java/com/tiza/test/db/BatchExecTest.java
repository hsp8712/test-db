package com.tiza.test.db;

import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;

public class BatchExecTest {

    public static void main(String[] args) {

        Connection conn = null;
        Statement stmt = null;

        try {
            conn = DBUtils.getConnection();
            conn.setAutoCommit(false);

            stmt = conn.createStatement();
            stmt.addBatch("INSERT INTO batch_test(id) VALUES (4)");
            stmt.addBatch("INSERT INTO batch_test(id) VALUES (5)");
            stmt.addBatch("INSERT INTO batch_test(id) VALUES (1)");
            stmt.addBatch("INSERT INTO batch_test(id) VALUES (6)");

            stmt.executeBatch();
            System.out.println("executeBatch");

            conn.commit();
            System.out.println("commit");

        } catch (BatchUpdateException e) {
            e.printStackTrace();
            System.out.println("UpdateCounts -> " + Arrays.toString(e.getUpdateCounts()));

            // **********************
            try {
                conn.commit();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            // **********************

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.close(conn, stmt);
        }

    }
}

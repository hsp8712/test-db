package com.tiza.test.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Properties;

/**
 * Global configuration class.
 *
 * @author huangshaoping
 */
public class Configuration {

    public static final Logger LOG = LoggerFactory
            .getLogger(Configuration.class);

    private static final Configuration conf = new Configuration();
    private Properties p = new Properties();

    private Configuration() {
    }

    public static Configuration getInstance() {
        return conf;
    }

    public void load(String fileName) {
        try {
            p.load(Thread.currentThread().getContextClassLoader()
                    .getResourceAsStream(fileName));
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
            LOG.error("Configuration Load failed.");
            System.exit(-1);
        }
    }

    public String getConfig(String key) {
        String conf = p.getProperty(key);
        conf = (conf == null ? null : conf.trim());
        return conf;
    }

    public String getConfig(String key, String defaultVal) {
        String conf = p.getProperty(key, defaultVal);
        conf = (conf == null ? null : conf.trim());
        return conf;
    }
}

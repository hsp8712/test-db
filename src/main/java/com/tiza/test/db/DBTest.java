package com.tiza.test.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class DBTest {

    public static final Logger LOG = LoggerFactory
            .getLogger(DBTest.class);


    private int count = 0;


    private Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        DBTest test = new DBTest();
        test.test();

    }

    public void test() {
        count++;

        LOG.info("index[{}], query...", count);

        Connection conn = null;
        Statement stmt = null;

        long t1 = System.currentTimeMillis();

        try {
            conn = DBUtils.getConnection();

            System.out.println(conn.getMetaData().getDatabaseProductName());

            stmt = conn.createStatement();
            stmt.executeQuery("select 1 from dual");
            long t2 = System.currentTimeMillis();

//            String cmd = null;
//            cmd = scan.nextLine();

            LOG.info("index[{}], select 1, spend time [{}]ms", count, (t2 - t1));

//            try {
//                Thread.sleep(200);
//            } catch (InterruptedException e) {
//            }

        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
            }
            LOG.info("release");
        }

    }
}

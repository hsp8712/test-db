package com.tiza.test.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class DBTest2 {

    public static final Logger LOG = LoggerFactory
            .getLogger(DBTest2.class);


    public static void test() {

        Connection conn = null;
        Statement stmt = null;

        long t1 = System.currentTimeMillis();

        try {
            conn = DBUtils.getConnection();
            conn.setAutoCommit(false);
            stmt = conn.createStatement();

            for (int i = 0; i < 5; i++) {
                stmt.addBatch("insert into t1 values(" + i + ",'ccc')");
            }

            stmt.executeBatch();
            long t2 = System.currentTimeMillis();

            LOG.info("Statement spend time [{}]ms", (t2 - t1));

        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
            }
            LOG.info("release");
        }
    }

    public static void test1() {

        Connection conn = null;
        PreparedStatement stmt = null;

        long t1 = System.currentTimeMillis();

        try {
            conn = DBUtils.getConnection();
            stmt = conn.prepareStatement("INSERT INTO date_test VALUES (?)");
            stmt.setTimestamp(1, new Timestamp(-125744875193000L));
            stmt.executeUpdate();
        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException e) {
                }
            }

            if(conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
            }
            LOG.info("release");
        }
    }


    public static void main(String[] args) throws InterruptedException {
//        test();
        test1();
    }
}

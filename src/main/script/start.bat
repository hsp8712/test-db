@echo off

cd /d %~dp0
cd ..
set "PRJ_HOME=%cd%"
echo PRJ_HOME:%PRJ_HOME%

set "LOG_HOME=%PRJ_HOME%\logs"
echo LOG_HOME:%LOG_HOME%

setlocal EnableDelayedExpansion
set CLASSPATH=%PRJ_HOME%\conf
for /F %%j in ('dir /s/b lib\*.jar') do set CLASSPATH=!CLASSPATH!;%%j

echo CLASSPATH:%CLASSPATH%

if exist "%LOG_HOME%" goto jvmRun
mkdir %LOG_HOME%

:jvmRun
java -classpath %CLASSPATH% -Dlogging.home=%LOG_HOME% com.tiza.ngp.kafka.consumer.KafkaConsumerMain
这是一篇没写完的文章，2015年的事就不留到2016了

# 问题复现

### 开启DNS解析
````bash
mysql> show variables like 'skip_name_resolve';
+-------------------+-------+
| Variable_name     | Value |
+-------------------+-------+
| skip_name_resolve | OFF   |
+-------------------+-------+
````

通过代码模拟用户查询请求，不断的建立连接执行sql查询，然后通过`show processlist`命令查看连接情况：

JAVA代码一
````java
/**
 *  MySQL Problem DNS lead to 'unauthenticated  user'
 */
public class MysqlDNSUnauthUser {

    public static final Logger LOG = LoggerFactory
            .getLogger(MysqlDNSUnauthUser.class);
    /**
     * 执行查询操作
     * @return
     */
    public static long query() {

        long start = System.currentTimeMillis();
        Connection conn = null;
        Statement stmt = null;

        String sql = "select 1";
        try {
            // 创建新的连接
            conn = DBUtils.getConnection();
            stmt = conn.createStatement();
            stmt.executeQuery(sql);

        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        } finally {
            DBUtils.close(conn, stmt);
        }

        long end = System.currentTimeMillis();
        return end - start;
    }

    public static void main(String[] args) {

        // 描述统计
        DescriptiveStatistics stats = new DescriptiveStatistics();
        int n = 0;
        while(true) {
            n++;
            long time = query();
            stats.addValue(time);
            if(n == 1000) break;
        }

        LOG.info(stats.toString());
    }
}
````

打印结果如下：
````
n: 1000
min: 10.0
max: 421.0
mean: 14.202000000000002
std dev: 16.242444797905648
median: 12.0
skewness: 20.270437547223214
kurtosis: 463.94461541900915
````

发现最大值421ms

````bash
mysql> show processlist;
+------+----------------------+-------------------+------+---------+------+-------+------------------+
| Id   | User                 | Host              | db   | Command | Time | State | Info             |
+------+----------------------+-------------------+------+---------+------+-------+------------------+
|    7 | root                 | localhost         | test | Query   |    0 | NULL  | show processlist |
| 2763 | unauthenticated user | 192.168.4.2:54804 | NULL | Connect | NULL | login | NULL             |
+------+----------------------+-------------------+------+---------+------+-------+------------------+
````
可以发现其中出现`unauthenticated user`的用户

### 关闭DNS解析
在`/etc/my.cnf`中的`[mysqld]`节点增加`skip_name_resolve`，重启mysqld服务
````bash
mysql> show variables like 'skip_name_resolve';
+-------------------+-------+
| Variable_name     | Value |
+-------------------+-------+
| skip_name_resolve | ON    |
+-------------------+-------+
````

再次执行JAVA代码一，结果如下：
````
n: 1000
min: 7.0
max: 413.0
mean: 10.306999999999988
std dev: 16.131117877070835
median: 9.0
skewness: 20.655330340756084
kurtosis: 471.9124123214076
````
对比上一次开启DNS解析，这次的结果性能有所提高，上一次的均值是14ms，这次为10ms
````
mysql> show processlist;
+------+----------------------+-------------------+------+---------+------+-------+------------------+
| Id   | User                 | Host              | db   | Command | Time | State | Info             |
+------+----------------------+-------------------+------+---------+------+-------+------------------+
|    2 | root                 | localhost         | NULL | Query   |    0 | NULL  | show processlist |
| 3891 | unauthenticated user | 192.168.4.2:59502 | NULL | Connect | NULL | login | NULL             |
+------+----------------------+-------------------+------+---------+------+-------+------------------+
````
可以发现仍然出现了`unauthenticated user`的用户

目前可得出如下结论：
> DNS解析确实会影响连接建立的性能，但是DNS解析开启与否，与出现`unauthenticated user`没有关系

猜想`unauthenticated user`的出现应该是连接在建立的过程中出现的临时状态


